from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse

from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

app = FastAPI()
app.title = "Mi aplicación con  FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)
Base.metadata.create_all(bind=engine)

class Config:
    schema_extra = {
        "example": {
            "id": 1,
            "title": "Mi película",
            "overview": "Descripción de la película",
            "year": 2022,
            "rating": 9.8,
            "category" : "Acción"
            }
        }


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')