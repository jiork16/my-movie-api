from models.movie import Movie as MovieModel
from schemas.movie import Movie
class MovieService():
  def __init__(self, db) -> None:
    self.db = db

  def get_movie(self):
    result = self.db.query(MovieModel).all()
    return result
  
  def get_movie_by_id(self, id):
    result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
    return result

  def get_movies_by_category(self, category:str):
    result = self.db.query(MovieModel).filter(MovieModel.category == category).first()
    return result
 
  def create_movie(self, movie: Movie):
    new_movie = MovieModel(**movie.dict())
    self.db.add(new_movie)
    self.db.commit()
    return new_movie
  
  def update_movie(self, id: int, movie: Movie):
    update_movie = self.db.query(MovieModel).filter(MovieModel.id ==id).first()
    update_movie.title = movie.title
    update_movie.overview = movie.overview
    update_movie.year = movie.year
    update_movie.rating = movie.rating
    update_movie.category = movie.category
    self.db.commit()
    return

  def delete_movie(self, id: int):
    self.db.query(MovieModel).filter(MovieModel.id ==id).delete()
    self.db.commit()
    return